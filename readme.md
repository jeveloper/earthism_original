Earthism.net 
---------------
I created this project as part of Challengepost WorldBank Challenge to create a product for the betterment of the people. This is what i built:

  - Main idea: Using world bank data, i raise awareness through visualizing data using Google Vizualization api (e.g. Education for females) 
  - "Give me a man tool" , Kiva is an amazing microlending site for tiny businesses , often in poor countries
  - By selecting a country on a map, one will see a list of females/males micro-businesses and now has a chance to help someone by funding their initiative 


** This project is built on PHP Framework Codeigniter , jquery , google visualization api, and html5, world bank data api is available through JSON or XML format.**


