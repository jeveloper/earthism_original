<!doctype html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="google-site-verification" content="YLO4UMNwLA9CwAbzHF6Xb0qrnLKFVO-2bBCUTzGj1hA" />
	<title>Awareness and Help</title>
	<meta name="description" content="Connecting World Bank Data with kiva micro loan seekers">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
	
	
	    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js" type="text/javascript"></script> 
	
	<script src="<?= base_url();?>public/js/libs/modernizr-2.0.min.js"></script>
	<script src="<?= base_url();?>public/js/libs/respond.min.js"></script>
	 <script src="https://www.google.com/jsapi?key=ABQIAAAAVmrWynIZVux2e7i2mL_7DRRbjCxXzbuRl0K74x-OZqRl-zsRVRRsq2zfUsWyHmMcb49en-RmjZfeMQ" type="text/javascript"></script>
     
    
      
      <script type="text/javascript" src="<?= base_url();?>public/js/libs/jquery.tmpl.min.js" charset="utf-8"></script>
      <script id="kiva_item" type="text/html">
 
    <tr><td><div>
                 Name: ${name} <br/>
                 Location: ${location} <br/>
                 Loan status: ${status}
                </div>
            </td>
            <td>  <div>
                 Use: ${use} <br/>
                 Activity: ${activity} 
                 
                </div></td>
            <td>${sector}</td>
            <td><a href="http://www.kiva.org/lend/${id}" target="_blank">Kiva website</a></td>
        </tr>
</script>
  <script type='text/javascript'>
     google.load('visualization', '1', {'packages': ['geomap','gauge']});   
   //google.setOnLoadCallback(drawMap);

   
   
    
    var data;
     var container  ;      
       var geomap ;
    $(document).ready (function() {
   
        
     data = new google.visualization.DataTable();    
     container = document.getElementById('map_canvas');
     geomap = new google.visualization.GeoMap(container); 
     
     $("#map_options").hide();
     $("#kiva_loans").hide();        
    });

  
   
    function drawMap(raw_data,indicator_label,region) {
      data = new google.visualization.DataTable();  
      data.addRows(raw_data.length);
      data.addColumn('string', 'Country');
      data.addColumn('number', "");
      data.addColumn('string', 'HOVER', 'HoverText');
      
      var index = 0;
      $.each(raw_data, function(i,value) { 
              data.setValue(index, 0, value.record.countrycode);
              data.setValue(index, 1, value.record.value);
              data.setValue(index, 2, value.record.country);         
              
              index++;   
            });
      
             

      var options = {};
      options['dataMode'] = 'regions';
      options['width'] = "640px";
      options['height'] = "380px";
      
      options['showLegend'] = "true";
      
      options['region'] = region;
      
        
      google.visualization.events.addListener(geomap, 'select', function() {
          var selection = geomap.getSelection();
          var item = selection[0];
      
      //data.getValue(rowIndex, columnIndex)  format  
          
      //data.getValue(item.row, 0); //country      
      
      $('#countrycode_sel').val(data.getValue(item.row, 0));
      $('#country_label').text("( "+data.getValue(item.row, 2)+ " )"); //country name
      
       loadLoans();

    });
   
      google.visualization.events.addListener(geomap, 'drawingDone', function(){$("#map_options").show();});    //load only when done
      geomap.draw(data, options);
       
  };
  
  function switchRegion(region){
     var options = {};
      options['dataMode'] = 'regions';
      options['width'] = "640px";
      options['height'] = "380px";
      
      options['showLegend'] = "true";
      
      options['region'] = region;
      
    
     geomap.draw(data, options); 
  }
  

  
  
      
      
      
      function refreshMap(){
        
          
          var indicator = $('#indicator option:selected').val(); 
          var ind_label = $('#indicator option:selected').text(); 
          var date = $('#year option:selected').val(); 
          
      
          
          //"AG.LND.ARBL.ZS"
          
       $.post("http://earthism.net/finder/loadDataByIndicator",
          {             
            per_page: "240",
            date: date,
            indicator: indicator,
            countrycodes: "all"
            
          },
          function(data) {
              
              if (data.length > 1){          
                drawMap(data,ind_label,"world");     
              }else{
                alert("No World Bank data provided.");
              }
          },"json"); //get JSON
          
      }
      
      
      function loadLoans(){
        $( "#kiva_item" ).template( "item_tmpl" );
        $("#kiva_list").empty();  
               
           $.post("http://earthism.net/finder/searchLoans",
          {             
            country: $('#countrycode_sel').val(),
            gender:  $('#gender').val()
            
          },
          function(data) {
               var index = 0;
                $("#nokiva").html("");
                
                
             if (data.length == 0){
                $("#kiva_loans").hide();
                $("#nokiva").html("Kiva did not find loans for selected country");
             }else{
              $("#kiva_loans").show();
                $.each(data, function(i,value) { 
               
               
                   var record = { id: value.record.id, name: value.record.name, status: value.record.status, use: value.record.use, activity: value.record.activity,location:value.record.location,sector: value.record.sector};
                    $.tmpl("item_tmpl",record).appendTo("#kiva_list");  
                  
                  index++;   
                });  
             } 
                
             
                                   
                 
                 
          },"json"); //get JSON
                     
      
      }//loadLoans
      
       function filterLoans(gender){
           $('#gender').val(gender);
           loadLoans();
       }
   
    
       
  </script>

</head>
<body>
	<div id="header-container">
		<header class="wrapper">
			<h1 id="title">Earthism</h1>
			<nav>
				<ul>
					<li><a href="http://www.jeveloper.com">Developer Site</a></li>
					<li><a href="http://www.kiva.org">Kiva MicroLoans</a></li>
					
									</ul>
			</nav>
		</header>
	</div>
	<div id="main" class="wrapper">
		<aside>
			
		<h3>1. Select<br/>2. Click</br> 3. Help</h3>
					</aside>
		<article>
			<header>
				<h2>Start</h2>
				<p>
					
<select  id="year" ><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option></select>
<select  id="indicator" >
<option value="SE.PRM.CMPT.FE.ZS" >Primary completion rate, female(%)</option>
<option value="SP.DYN.LE00.FE.IN" >Life expectancy at birth, female (years)</option>
<option value="SL.AGR.EMPL.FE.ZS" >Employees, agriculture, female(%)</option>
<option value="SL.UEM.LTRM.FE.ZS" >Long-term unemployment, female(%)</option>
<option value="SL.EMP.VULN.FE.ZS" >Vulnerable employment, female (%)</option>
<option value="SL.SRV.EMPL.FE.ZS" >Employees, services, female</option>
<option value="SE.ADT.1524.LT.FE.ZS" >Literacy rate, youth female (%)</option>
<option value="IT.NET.USER" >Internet Users</option>
</select>

<button type="submit" class="button" id="searchsubmit" onclick="refreshMap();">Start by clicking me</button>


<div id='map_canvas'></div> <div id='chart_div'></div>
  
   <div id="map_options">
        <p>View by:</p>
       <a href="#" onclick="switchRegion('002');">Africa</a>
       <a href="#" onclick="switchRegion('005');">South America</a>
       <a href="#" onclick="switchRegion('035');">Asia/Pacific</a>
       <a href="#" onclick="switchRegion('013');">Central America</a>
       <a href="#" onclick="switchRegion('145');">Middle East</a>
       <a href="#" onclick="switchRegion('155');">Europe</a>  
       <a href="#" onclick="switchRegion('world');">World</a>
       <br/><strong>Click on a country to See Kiva Loan seekers</strong> 
    </div>   
     
   
  
   <div>
       <img src="http://l3-1.kiva.org/r29796/images/logoLeafy3.gif" border="0"/>
       <img src="http://l3-2.kiva.org/r29796/images/tagline.gif" border="0"/>&nbsp;<strong id="country_label"></strong>
      <br/>
        <p id="nokiva" style="color:#900000; font-size:20px;" align="center"></p>  
   </div>
   
   <div id="kiva_loans">   
    <h3>Entrepreneur  seeking micro loans to start a business</h3>
    <p>
    <a href="#kiva_loans" onclick="filterLoans('female');">Women Entrepreneurs</a>&nbsp;
    <a href="#kiva_loans" onclick="filterLoans('male');">Men Entrepreneurs</a>
    </p> 

   <table id="hor-minimalist-b" >
    <thead>
        <tr>
            <th scope="col">Person</th>
            <th scope="col">Purpose</th>
            <th scope="col">Sector</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody id="kiva_list">
       
    </tbody>
</table>

       <input type="hidden" id="countrycode_sel" value=""/>
       <input type="hidden" id="gender" value="female,male"/>

   
				</p>
			</header>
			<footer>
			<h3>About the project</h3>
				<p>World Bank Data provides hundres of indicators, this application uses this data to show world problems using intensity map. Kiva a famous non-profit organization allows us to show tiny business that are seeking loans.</p>
			</footer>
		</article>
	</div>
	<div id="footer-container">
		<footer class="wrapper">
			<h3>Developed in PHP MVC, Jquery, HTML5, Google Libraries</h3>
		</footer>
	</div>

<!-- scripts concatenated and minified via ant build script-->
<script src="<?= base_url();?>public/js/plugins.js"></script>

<!-- end scripts-->
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-368602-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
